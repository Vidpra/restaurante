<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Alergeno;

class AlergenoTest extends TestCase
{
    use DatabaseTransactions;

    /*
    * Confirmar que se puede crear un alergeno
    */
    public function testCrearAlergeno()
    {
        factory(Alergeno::class)->create();
    }

    /*
    * El API tiene que crear un alergeno
    */
    public function testApiCreaAlergeno()
    {
        $alergeno = factory(Alergeno::class)->make();

        $this->json('POST', 'api/alergenos', $alergeno->toArray())
            ->seeJson([
                'created' => true,
            ]);
    }

    /*
    * El API tiene que devolver un listado de los alergenos
    */
    public function testApiDevuelveListadoAlergenos()
    {
        factory(Alergeno::class, 5)->create();

        $this->get('api/alergenos')
            ->seeJsonStructure([
                'alergenos' => []
            ]);
    }
}

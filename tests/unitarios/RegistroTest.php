<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Plato;
use App\Ingrediente;
use App\Alergeno;

class RegistroTest extends TestCase
{
    use DatabaseTransactions;


    /*
    * El API tiene que devolver un registro de las ultimas modificaciones de los ingredientes de un plato
    */
    public function testApiRegistroIngredientesPlato()
    {
        factory(Alergeno::class, 10)->create();

        factory(Ingrediente::class, 10)->create()->each(function($ingrediente) {
            $ingrediente->alergenos()->sync( Alergeno::get()->pluck('id')->shuffle()->take(rand(0,2))->toArray() );
        });

        $plato = factory(Plato::class)->create();

        $plato->ingredientes()->sync( Ingrediente::get()->pluck('id')->shuffle()->take(2)->toArray() );

        $this->get("api/platos/$plato->id")
            ->seeJsonStructure([
                'alergenos' => [],
                'ingredientes' => [],
            ]);
    }
}

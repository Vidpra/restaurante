<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Plato;
use App\Ingrediente;
use App\Alergeno;

class PlatoTest extends TestCase
{
    use DatabaseTransactions;

    /*
    * Confirmar que se puede crear un plato
    */
    public function testCrearPlato()
    {
        factory(Plato::class)->create();
    }

    /*
    * El API tiene que crear un plato
    */
    public function testApiCreaPlato()
    {
        $plato = factory(Plato::class)->make();

        $this->json('POST', 'api/platos', $plato->toArray())
            ->seeJson([
                'created' => true,
            ]);
    }

    /*
    * El API tiene que devolver un listado de los platos
    */
    public function testApiDevuelveListadoPlatos()
    {
        factory(Plato::class, 5)->create();

        $this->get('api/platos')
            ->seeJsonStructure([
                'platos' => []
            ]);
    }

    /*
    * El API tiene que crear un plato que contenga varios ingredientes
    */
    public function testApiCreaPlatoConIngredientes()
    {
        $plato = factory(Plato::class)->make();

        $plato->ingredientes = factory(Ingrediente::class, 10)->create()
            ->pluck('id')->shuffle()->take(rand(1,5));

        $this->json('POST', 'api/platos', $plato->toArray())
            ->seeJson([
                'created' => true,
            ]);
    }

    /*
    * El API tiene que actualizar los ingredientes de un plato
    */
    public function testApiAtualizaIngredientesPlato()
    {
        $plato = factory(Plato::class)->create();

        $ingredientes = factory(Ingrediente::class, 10)->create()
            ->pluck('id')->shuffle()->take(rand(1,5));

        $this->json('PUT', "api/platos/$plato->id", $ingredientes->toArray())
            ->seeJson([
                'updated' => true,
            ]);
    }

    /*
    * El API tiene que devolver el listado de alergenos de un plato especifico
    */
    public function testApiDevuelveAlergenosDePlato()
    {
        factory(Alergeno::class, 10)->create();

        factory(Ingrediente::class, 10)->create()->each(function($ingrediente) {
            $ingrediente->alergenos()->sync( Alergeno::get()->pluck('id')->shuffle()->take(rand(0,2))->toArray() );
        });

        $plato = factory(Plato::class)->create();

        $plato->ingredientes()->sync( Ingrediente::get()->pluck('id')->shuffle()->take(rand(1,5))->toArray() );

        $this->get("api/platos/$plato->id")
            ->seeJsonStructure([
                'alergenos' => []
            ]);
    }

    /*
    * El API tiene que devolver el listado de platos en los que aparece un alergeno especifico
    */
    public function testApiDevuelvePlatosConAlergenoEspecifico()
    {
        factory(Alergeno::class, 10)->create();

        factory(Ingrediente::class, 10)->create()->each(function($ingrediente) {
            $ingrediente->alergenos()->sync( Alergeno::get()->pluck('id')->shuffle()->take(rand(0,2))->toArray() );
        });

        $plato = factory(Plato::class)->create()->each(function($plato) {
            $plato->ingredientes()->sync( Ingrediente::get()->pluck('id')->shuffle()->take(rand(1,5))->toArray() );
        });

        $this->get('api/alergenos/1')
            ->seeJsonStructure([
                'platos' => []
            ]);
    }
}

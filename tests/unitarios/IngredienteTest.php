<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Ingrediente;
use App\Alergeno;

class IngredienteTest extends TestCase
{
    use DatabaseTransactions;

    /*
    * Confirmar que se puede crear un ingrediente
    */
    public function testCrearIngrediente()
    {
        factory(Ingrediente::class)->create();
    }

    /*
    * El API tiene que crear un ingrediente
    */
    public function testApiCreaIngrediente()
    {
        $ingrediente = factory(Ingrediente::class)->make();

        $this->json('POST', 'api/ingredientes', $ingrediente->toArray())
            ->seeJson([
                'created' => true,
            ]);
    }

    /*
    * El API tiene que devolver un listado de los ingredientes
    */
    public function testApiDevuelveListadoIngredientes()
    {
        factory(Ingrediente::class, 5)->create();

        $this->get('api/ingredientes')
            ->seeJsonStructure([
                'ingredientes' => []
            ]);
    }

    /*
    * El API tiene que devolver un ingrediente especifico
    */
    public function testApiDevuelveIngrediente()
    {
        factory(Ingrediente::class)->create();

        $this->get('api/ingredientes/1')
            ->seeJsonStructure([
                'nombre'
            ]);
    }

    /*
    * El API tiene que crear un ingrediente que contega varios alergenos
    */
    public function testApiCreaIngredienteConAlergenos()
    {
        $ingrediente = factory(Ingrediente::class)->make();

        $ingrediente->alergenos = factory(Alergeno::class, 10)->create()
            ->pluck('id')->shuffle()->take(rand(0,2));

        $this->json('POST', 'api/ingredientes', $ingrediente->toArray())
            ->seeJson([
                'created' => true,
            ]);
    }

    /*
    * El API tiene que actualizar los alergenos de un ingrediente
    */
    public function testApiAtualizaAlergenosIngrediente()
    {
        $ingrediente = factory(Ingrediente::class)->create();

        $alergenos = factory(Alergeno::class, 10)->create()
            ->pluck('id')->shuffle()->take(rand(0,2));

        $this->json('PUT', "api/ingredientes/$ingrediente->id", $alergenos->toArray())
            ->seeJson([
                'updated' => true,
            ]);
    }
}

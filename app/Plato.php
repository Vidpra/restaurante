<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plato extends Model
{
    /**
     * Atributos asignables
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * Atributos no devueltos en las consultas
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    // La relacion entre platos e ingredientes es Many to Many
    public function ingredientes()
    {
        return $this->belongsToMany('App\Ingrediente')
            ->withTimestamps();
    }
}

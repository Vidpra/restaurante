<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alergeno extends Model
{
    /**
     * Atributos asignables
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * Atributos no devueltos en las consultas
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Indicar si el modelo necesita guardar fechas de creacion y actualizacion
     *
     * @var bool
     */
    public $timestamps = false;

    // La relacion entre alergenos e ingredientes es Many to Many
    public function ingredientes()
    {
        return $this->belongsToMany('App\Ingrediente');
    }
}

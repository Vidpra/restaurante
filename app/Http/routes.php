<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// API 1.2.1
Route::group(['prefix' => 'api'], function(){

    Route::resource('platos', 'PlatoController');
    Route::resource('ingredientes', 'IngredienteController');
    Route::resource('alergenos', 'AlergenoController');

});

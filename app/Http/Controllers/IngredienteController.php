<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ingrediente;

class IngredienteController extends Controller
{
    /**
     * Listado de todos los ingredientes
     *
     * @return Response json
     */
    protected function index()
    {
        $ingredientes = Ingrediente::all()->pluck('nombre', 'id');

        if ( $ingredientes->isEmpty() )
        {
            return response()->json([
                'error' => 'No se han encontrado ingredientes'
            ], 404);
        }

        return response()->json([
            'ingredientes' => $ingredientes
        ], 200);
    }

    /**
     * Crear un ingrediente con alergenos
     *
     * @param  Request $request
     * @return Response json
     */
    protected function store(Request $request)
    {
        $ingrediente = Ingrediente::firstOrCreate([
            'nombre' => $request->input('nombre')
        ]);

        if ($request->has('alergenos')) {
            $ingrediente->alergenos()->sync($request->input('alergenos'));
        }

        return response()->json([
            'created' => true,
        ], 201);
    }

    /**
     * Actualizar los alergenos de un ingrediente existente
     *
     * @param  Request $request, $id
     * @return Response json
     */
    protected function update(Request $request, $id)
    {
        $ingrediente = Ingrediente::find($id);

        if ( ! $ingrediente )
        {
            return response()->json([
                'error' => 'Ingrediente no encontrado'
            ], 400);
        }

        if ($request->has('alergenos')) {
            $ingrediente->alergenos()->sync($request->input('alergenos'));
        }

        return response()->json([
            'updated' => true,
        ], 200);
    }

    /**
     * Ver un ingrediente
     *
     * @param  $id
     * @return Response
     */
    protected function show($id)
    {
        $ingrediente = Ingrediente::with(['alergenos' => function($query) use (&$alergenos) {
            $alergenos = $query->get()->unique()->pluck('nombre', 'id');
        }])->find($id);

        if ( ! $ingrediente )
        {
            return response()->json([
                'error' => 'Ingrediente no encontrado'
            ], 404);
        }

        return response()->json([
            'nombre' => $ingrediente->nombre,
            'alergenos' => $alergenos
        ], 200);
    }
}

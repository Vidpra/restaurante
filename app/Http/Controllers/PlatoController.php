<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Plato;

class PlatoController extends Controller
{
    /**
     * Listado de todos los platos
     *
     * @return Response json
     */
    protected function index()
    {
        $platos = Plato::all();

        if ( $platos->isEmpty() )
        {
            return response()->json([
                'error' => 'No se han encontrado platos'
            ], 404);
        }

        return response()->json([
            'platos' => $platos
        ], 200);
    }

    /**
     * Ver alergenos de un plato
     *
     * @param  $id
     * @return Response json
     */
    protected function show($id)
    {
        $plato = Plato::with(['ingredientes.alergenos' => function($query) use (&$alergenos) {
            $alergenos = $query->get()->unique()->pluck('nombre', 'id');
        }])->find($id);

        foreach ($plato->ingredientes as $ingrediente) {
            $ingrediente->fecha = $ingrediente->pivot->updated_at->toDateTimeString();
        }

        if ( ! $plato )
        {
            return response()->json([
                'error'  => 'Plato no encontrado'
            ], 404);
        }

        return response()->json([
            'nombre' => $plato->nombre,
            'alergenos' => $alergenos,
            'ingredientes' => $plato->ingredientes->pluck('fecha','nombre')
        ], 200);
    }

    /**
     * Crear un plato con ingredientes
     *
     * @param  Request $request
     * @return Response json
     */
    protected function store(Request $request)
    {
        $plato = Plato::firstOrCreate([
            'nombre' => $request->input('nombre')
        ]);

        if ($request->has('ingredientes')) {
            $plato->ingredientes()->sync($request->input('ingredientes'));
        }

        $plato->save();

        return response()->json([
            'created' => true,
        ], 201);
    }

    /**
     * Actualizar los ingredientes de un plato existente
     *
     * @param  Request $request, $id
     * @return Response json
     */
    protected function update(Request $request, $id)
    {
        $plato = Plato::find($id);

        if ( ! $plato )
        {
            return response()->json([
                'error' => 'Plato no encontrado'
            ], 400);
        }

        if ($request->has('ingredientes')) {
            $plato->ingredientes()->sync($request->input('ingredientes'));
        }

        return response()->json([
            'updated' => true,
        ], 200);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Alergeno;
use App\Plato;

class AlergenoController extends Controller
{
    /**
     * Listado de todos los alergenos
     *
     * @return Response json
     */
    protected function index()
    {
        $alergenos = Alergeno::all()->pluck('nombre', 'id');

        if ( $alergenos->isEmpty() )
        {
            return response()->json([
                'error' => 'No se han encontrado alergenos'
            ], 404);
        }

        return response()->json([
            'alergenos' => $alergenos
        ], 200);
    }

    /**
     * Crear un alergeno
     *
     * @param  Request $request
     * @return Response json
     */
    protected function store(Request $request)
    {
        $alergeno = Alergeno::create([
            'nombre' => $request->input('nombre')
        ]);

        if ( ! $alergeno )
        {
            return response()->json([
                'error' => 'No se ha podido crear el  alergeno'
            ], 400);
        }

        return response()->json([
            'created' => true,
        ], 201);
    }

    /**
     * Ver platos en los que aparece un alergeno especifico
     *
     * @param  $id
     * @return Response json
     */
    protected function show($id)
    {
        $alergeno = Alergeno::with(['ingredientes.platos' => function($query) use (&$platos) {
           $platos = $query->get()->pluck('nombre', 'id');
       }])->find($id);

       if ( ! $alergeno )
       {
           return response()->json([
               'error' => 'Alergeno no encontrado'
           ], 404);
       }

        return response()->json([
            'nombre' => $alergeno->nombre,
            'platos' => $platos
        ], 200);
    }
}

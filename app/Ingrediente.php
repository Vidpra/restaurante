<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    /**
     * Atributos asignables
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * Atributos no devueltos en las consultas
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Indicar si el modelo necesita guardar fechas de creacion y actualizacion
     *
     * @var bool
     */
    public $timestamps = false;

    // La relacion entre ingredientes y platos es Many to Many
    public function platos()
    {
        return $this->belongsToMany('App\Plato')
            ->withTimestamps();
    }

    // La relacion entre ingredientes y alergenos es Many to Many
    public function alergenos()
    {
        return $this->belongsToMany('App\Alergeno');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
| https://github.com/fzaninotto/Faker
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Plato::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->unique()->sentence($nbWords = 3, $variableNbWords = true),
    ];
});

$factory->define(App\Ingrediente::class, function (Faker\Generator $faker) {
    return [
        'nombre' => ucfirst($faker->unique()->word),
    ];
});

$factory->define(App\Alergeno::class, function (Faker\Generator $faker) {
    return [
        'nombre' => ucfirst($faker->unique()->word),
    ];
});

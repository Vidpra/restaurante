<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientePlatoTable extends Migration
{
    public function up()
    {
        Schema::create('ingrediente_plato', function (Blueprint $table) {
            $table->integer('ingrediente_id')->unsigned();
            $table->foreign('ingrediente_id')->references('id')->on('ingredientes')->onDelete('cascade');
            $table->integer('plato_id')->unsigned();
            $table->foreign('plato_id')->references('id')->on('platos')->onDelete('cascade');
            $table->unique(['ingrediente_id', 'plato_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ingrediente_plato');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlergenoIngredienteTable extends Migration
{
    public function up()
    {
        Schema::create('alergeno_ingrediente', function (Blueprint $table) {
            $table->integer('alergeno_id')->unsigned();
            $table->foreign('alergeno_id')->references('id')->on('alergenos')->onDelete('cascade');
            $table->integer('ingrediente_id')->unsigned();
            $table->foreign('ingrediente_id')->references('id')->on('ingredientes')->onDelete('cascade');
            $table->unique(['alergeno_id', 'ingrediente_id']);

        });
    }

    public function down()
    {
        Schema::drop('alergeno_ingrediente');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlergenosTable extends Migration
{
    public function up()
    {
        Schema::create('alergenos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });
    }

    public function down()
    {
        Schema::drop('alergenos');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AlergenosTableSeeder::class);
        $this->call(IngredientesTableSeeder::class);
        $this->call(PlatosTableSeeder::class);
    }
}

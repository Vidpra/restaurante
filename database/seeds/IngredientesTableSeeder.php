<?php

use Illuminate\Database\Seeder;
use App\Ingrediente;
use App\Alergeno;

class IngredientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Ingrediente::class, 20)->create()->each(function($ingrediente) {
            $ingrediente->alergenos()->sync( Alergeno::get()->pluck('id')->shuffle()->take(rand(0,2))->toArray() );
        });
    }
}

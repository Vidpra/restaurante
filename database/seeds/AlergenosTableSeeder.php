<?php

use Illuminate\Database\Seeder;
use App\Alergeno;

class AlergenosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Alergeno::class, 20)->create();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Plato;
use App\Ingrediente;

class PlatosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Plato::class, 10)->create()->each(function($plato) {
            $plato->ingredientes()->sync( Ingrediente::get()->pluck('id')->shuffle()->take(rand(1,5))->toArray() );
        });
    }
}

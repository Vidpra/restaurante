# Ejercicio del restaurante

## Tecnología
- PHP7
- MySQL
- Vagrant (para el servidor)
- Composer + NodeJS + npm + gulp (para instalaciones, configuraciones)
- PhpUnit (con abstracciones de Laravel)
- Postman (para tests manuales del API)
- Laravel PHP Framework [![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
- git

## Arquitectura
- RESTful API: controlada desde `routes.php`
- TDD: tests unitarios desde el comienzo `tests/unitarios`
- ORM: Modelos en `app`
- Generacion de tablas con migraciones `database/migrations`
- Seeds para popular la bbdd para los tests `database/seeds`
- ModelFactory para crear modelos con dummy data para los tests `database/factories`
- Controladores en `app/Http/Controllers`
- Artisan commands para el despliegue inicial

## Funcionalidades alcanzadas
- TDD / API: crear alergeno
- TDD / API: crear ingrediente
- TDD / API: crear ingrediente con alergenos
- TDD / API: actualizar alergenos de un ingrediente
- TDD / API: crear plato
- TDD / API: crear plato con ingredientes
- TDD / API: actualizar ingredientes de un plato
- TDD / API: devolver los alergenos de un plato
- TDD / API: devolver platos en los que aparece un alergeno concreto
- TDD / API: **BONUS** registro de cambios sobre los platos

## No implementado
- Clean Architecture sin depender de Laravel
- Tests de Integracion (no hay frontend)

## Decisiones
- No hay vistas, todo el proceso a sido backend usando TDD y Postman para verificar el API

REST API

- HTTP VERBS para determinar que acciones se realizaban
- Versionado de API prefijando las rutas (he usado "api", un mejor ejemplo seria "api/v1")
- Plurales para describir los recursos de las rutas
- JSON por defecto
- Codigos de respuesta (404, 200), mensajes de error

Cosas que se han quedado en el tintero para agilizar el ejercicio:

- El API no tiene middleware, no hay token authentication ni rate limiting
- No hay ningun tipo de Validador para los datos que llegan al API
- Las tablas estan por defecto, sin atributos unique ni nada por el estilo
- Los metodos tienen un comentario de cabecera que explica lo que se pretende pero no he detallado con comentarios lo que ocurre en su interior
- No he usado Eventos ni Disparadores para el tema del registro

## BONUS: Registro
- He añadido Timestamps tanto a los platos como a la tabla pivot que relaciona ingredientes y platos
- Al crear o modificar un plato se actualizan automaticamente los timestamps
- Al añadir nuevos ingredientes a un plato se actualizan automaticamente los timestamps
- Lo siguiente habria sido usar Eventos con alguna funcionalidad extra

Se muestran los ingredientes y su fecha de cambio en un plato.

Hay un ejemplo más adelante en la sección `Ejemplos de Respuestas del API en JSON`.

## Rutas del API

| Tipo    | API                                   | Controlador   |
| -------- | ----------- | -------------------------------------- |
| GET/HEAD  | /                                    |               |
| GET/HEAD  | api/alergenos                        |AlergenoController@index   |
| POST      | api/alergenos                        |AlergenoController@store   |
| GET/HEAD  | api/alergenos/create                 |AlergenoController@create  |
| PUT/PATCH | api/alergenos/{alergenos}            |AlergenoController@update  |
| GET/HEAD  | api/alergenos/{alergenos}            |AlergenoController@show    |
| DELETE    | api/alergenos/{alergenos}            |AlergenoController@destroy |
| GET/HEAD  | api/alergenos/{alergenos}/edit       |AlergenoController@edit    |
| GET/HEAD  | api/ingredientes                     |IngredienteController@index|
| POST      | api/ingredientes                     |IngredienteController@store |
| GET/HEAD  | api/ingredientes/create              |IngredienteController@create  |
| PUT/PATCH | api/ingredientes/{ingredientes}      |IngredienteController@update  |
| DELETE    | api/ingredientes/{ingredientes}      |IngredienteController@destroy |
| GET/HEAD  | api/ingredientes/{ingredientes}      |IngredienteController@show |
| GET/HEAD  | api/ingredientes/{ingredientes}/edit |IngredienteController@edit |
| GET/HEAD  | api/platos                           |PlatoController@index      |
| POST      | api/platos                           |PlatoController@store      |
| GET/HEAD  | api/platos/create                    |PlatoController@create     |
| DELETE    | api/platos/{platos}                  |PlatoController@destroy    |
| PUT/PATCH | api/platos/{platos}                  |PlatoController@update     |
| GET/HEAD  | api/platos/{platos}                  |PlatoController@show       |
| GET/HEAD  | api/platos/{platos}/edit             |PlatoController@edit       |

Las rutas a los siguientes controladores no estan implementadas:
- @destroy
- @edit


## Ejemplos de Respuestas del API

Respuesta de GET `api/platos`
```
{
  "platos": [
    {
      "id": 1,
      "nombre": "Qui distinctio sunt dolorem.",
      "created_at": "2016-05-19 05:41:23",
      "updated_at": "2016-05-19 05:41:23"
    },
    {
      "id": 2,
      "nombre": "Suscipit eveniet excepturi.",
      "created_at": "2016-05-19 05:41:23",
      "updated_at": "2016-05-19 05:41:23"
    }
}
```

Respuesta de GET `api/platos/1` (el nombre es del plato)
```
{
  "nombre": "Est illum cupiditate sed omnis.",
  "alergenos": {
    "1": "Consectetur",
    "7": "Corrupti",
    "8": "Deleniti",
    "16": "Quis",
    "18": "Qui",
    "19": "Labore"
  },
  "ingredientes": {
    "Sed": "2016-05-19 05:41:23",
    "Numquam": "2016-05-19 05:41:23",
    "Soluta": "2016-05-19 05:41:23",
    "Non": "2016-05-19 05:41:23",
    "Quod": "2016-05-19 05:41:23"
  }
}
```

Envio POST a `api/platos`
```
{
    "nombre": "Reiciendis ullam.",
    "ingredientes": [1,2,3,4,5]
}
```
Respuesta 200 OK al enviar POST a `api/platos`
```
{
  "created": true
}
```


Respuesta de GET `api/ingredientes`
```
{
  "ingredientes": {
    "1": "Nobis",
    "2": "Cumque",
    "3": "Minima",
    "4": "Sint",
    "5": "Aut"
  }
}
```

Respuesta de GET `api/ingredientes/1` (el nombre es del Ingrediente)
```
{
  "nombre": "Ut eaque.",
  "alergenos": {
    "7": "Delectus",
    "9": "Eum",
    "13": "Et"
  }
}
```

Respuesta de GET `api/alergenos`
```
{
  "alergenos": {
    "1": "Consectetur",
    "2": "Pariatur",
    "3": "Amet",
    "4": "Iure",
    "5": "Quas"
  }
}
```


Respuesta de GET `api/alergenos/1` (el nombre es del alergeno)
```
{
  "nombre": "Pariatur",
  "platos": {
    "3": "Facilis odio occaecati.",
    "4": "Itaque sunt sit esse."
  }
}
```


## Historial
Crear el proyecto `restaurante` con Laravel
```
laravel new restaurante
cd restaurante
```

Requerir Homestead (Vagrant Ubuntu con todo lo necesario)
```
composer require laravel/homestead --dev
composer update
vendor\\bin\\homestead make
```

Entrar en Homestead
```
vagrant up
vagrant ssh
cd /vagrant
```

Instalar paquetes npm y otras configuraciones
```
npm install
gulp
```

Crear proyecto `restaurante` en `GitLab` versión 1.0.0
```
git init
git remote add origin git@gitlab.com:Vidpra/restaurante.git
git add .
git commit -m "restaurante 1.0.0"
git push -u origin master
```

Crear migraciones
```
php artisan make:migration create_platos_table
php artisan make:migration create_ingredientes_table
php artisan make:migration create_alergenos_table
php artisan make:migration create_ingrediente_plato_table
php artisan make:migration create_alergeno_ingrediente_table
```

Crear modelos
```
php artisan make:model Plato
php artisan make:model Ingrediente
php artisan make:model Alergeno
```

Crear controladores
```
php artisan make:controller PlatoController
php artisan make:controller IngredienteController
php artisan make:controller AlergenoController
```

Crear seeds
```
php artisan make:seeder PlatosTableSeeder
php artisan make:seeder IngredientesTableSeeder
php artisan make:seeder AlergenosTableSeeder
```

Crear tests
```
php artisan make:test PlatoTest
php artisan make:test IngredienteTest
php artisan make:test AlergenoTest
php artisan make:test RegistroTest
```

Crear en `ModelFactory` modelos con dummy data

Crear en `routes.php` las rutas del `api`

Migrar/resetear la BBDD
`php artisan migrate:refresh`

Pupular con dummy data la BBDD
`php artisan db:seed`

Desarrollar la aplicacion
